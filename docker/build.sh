#!/bin/sh
# build.sh
# Builds the Docker image

TAG=$(cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];")

docker build \
-f docker/Dockerfile \
-t "nakachain/block-monitor:$TAG" \
.
