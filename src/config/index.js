const { isNaN } = require('lodash');

const Config = {
  URLS: [],
  SLACK_WEBHOOK_PATH: process.env.SLACK_WEBHOOK_PATH,
  MONITOR_INTERVAL_MS: 60000, // 60 secs
  MESSAGE_INTERVAL_MS: 900000, // 15 mins
};

/**
 * Initializes the config and validates the required env variables.
 */
const initConfig = () => {
  // Parse URLS list to array
  if (!process.env.URLS) throw Error('URLS missing in env');
  Config.URLS = process.env.URLS.split(',');

  if (!process.env.SLACK_WEBHOOK_PATH) throw Error('SLACK_WEBHOOK_PATH missing in env');

  if (process.env.MONITOR_INTERVAL_MS) {
    const monitorMs = Number(process.env.MONITOR_INTERVAL_MS);
    if (isNaN(monitorMs)) throw Error('MONITOR_INTERVAL_MS is not a number');
    if (monitorMs <= 0) throw Error('MONITOR_INTERVAL_MS should be greater than 0');
    Config.MONITOR_INTERVAL_MS = monitorMs;
  }

  if (process.env.MESSAGE_INTERVAL_MS) {
    const messageMs = Number(process.env.MESSAGE_INTERVAL_MS);
    if (isNaN(messageMs)) throw Error('MESSAGE_INTERVAL_MS is not a number');
    if (messageMs <= 0) throw Error('MESSAGE_INTERVAL_MS should be greater than 0');
    Config.MESSAGE_INTERVAL_MS = messageMs;
  }
};

module.exports = {
  Config,
  initConfig,
};
