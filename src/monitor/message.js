const axios = require('axios');
const { Config } = require('../config');
const logger = require('../util/logger');

axios.defaults.headers.post['Content-Type'] = 'application/json';

/**
 * Constructs the full Slack incoming webhook URL.
 * @return {string} Incoming webhook URL
 */
const constructURL = () => {
  return `https://hooks.slack.com/services/${Config.SLACK_WEBHOOK_PATH}`;
};

/**
 * Constructs the message text to post to the incoming webhook.
 * @param {string} url of the node that has the issue
 * @param {number} blockNum of the node that has the issue
 * @return {string} Text to post
 */
const constructText = (url, blockNum) => {
  return `Block Monitor: ${url} node stuck at block ${blockNum}`;
};

/**
 * Sends a POST request to the Slack incoming webhook.
 * @param {string} url of the node that has the issue
 * @param {number} blockNum of the node that has the issue
 */
const sendMessage = async (url, blockNum) => {
  try {
    logger.error(`Stuck block detected at ${url}. Sending message.`);
    await axios.post(constructURL(), {
      text: constructText(url, blockNum),
    });
  } catch (err) {
    logger.error('Send message error');
    throw err;
  }
};

module.exports = sendMessage;
