const { each } = require('async');
const { findIndex, isNil } = require('lodash');
const checkBlock = require('./check');
const sendMessage = require('./message');
const { Config } = require('../config');
const { sleep } = require('../util');
const logger = require('../util/logger');

/**
 * Stores the metadata about the last queried block nums.
 * {
 *    url: url of the node's RPC API
 *    blockNum: last queried block number
 *    lastMessage: UNIX timestamp in milliseconds of last message sent
 * }
 */
const metadata = [];

/**
 * Returns true if enough time has passed since the last message sent.
 * @param {number} time of last message sent
 * @return {boolean} True if enough time has passed
 */
const canSendMessage = (time) => {
  if (isNil(time)) return true;
  return new Date().getTime() - time >= Config.MESSAGE_INTERVAL_MS;
};

/**
 * Updates the metadata and handles if a node's blockNum is stuck.
 * @param {string} url of the node's RPC API
 * @param {number} blockNum of the node
 */
const handleBlockNumUpdate = async (url, blockNum) => {
  const index = findIndex(metadata, { url });
  if (index !== -1 && metadata[index].blockNum === blockNum) {
    if (canSendMessage(metadata[index].lastMessage)) {
      await sendMessage(url, blockNum);
      metadata[index].lastMessage = new Date().getTime();
    }
  } else if (index !== -1) {
    metadata[index].blockNum = blockNum;
  } else {
    metadata.push({ url, blockNum, lastMessage: null });
  }
};

/**
 * Starts the repeating monitor.
 */
const startMonitor = () => {
  each(
    Config.URLS,
    (url, next) => {
      logger.info(`Checking node: ${url}`);
      checkBlock(url).then((blockNum) => {
        if (isNil(blockNum)) {
          throw Error(`Error getting block number for ${url}`);
        }

        handleBlockNumUpdate(url, blockNum).then(() => {
          logger.info(`Finished: ${url} @ ${blockNum}`);
          next();
        }).catch((updateErr) => {
          next(updateErr);
        });
      }).catch((checkErr) => {
        next(checkErr);
      });
    },
    (err) => {
      if (err) {
        logger.error('Monitor error');
        throw err;
      }

      // Delay then restart monitor
      sleep(Config.MONITOR_INTERVAL_MS).then(() => {
        startMonitor();
      });
    },
  );
};

module.exports = startMonitor;
