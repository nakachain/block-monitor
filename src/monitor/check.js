const Web3 = require('web3');

/**
 * Checks the block number of the given node.
 * @param {string} url of the node's RPC API
 * @return {number} Block number of that node
 */
module.exports = async (url) => {
  const web3 = new Web3(url);
  return web3.eth.getBlockNumber();
};
