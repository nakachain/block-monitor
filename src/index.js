require('dotenv').config();
const { initConfig } = require('./config');
const startMonitor = require('./monitor');

initConfig();
startMonitor();
