# block-monitor

Monitors the blocks on specified servers and reacts to abnormalities.

## Environment Variables

```bash
# REQUIRED. Comma separated values of the RPC APIs to check.
URLS=https://url1.com,https://url2.com

# REQUIRED. Webhook URL to post a message to when a message is sent.
# This is the part after the base path: https://hooks.slack.com/services/
SLACK_WEBHOOK_PATH=ABCDEFGH/IJKLMNOPQ/rstuvwxyz

# Optional (30000). Number of milliseconds between checking block numbers.
MONITOR_INTERVAL_MS=30000

# Optional (900000). Minimum number of milliseconds between sending messages.
# Won't send the same message for the node unless this interval has passed.
MESSAGE_INTERVAL_MS=900000
```
